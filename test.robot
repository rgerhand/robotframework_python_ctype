*** Settings ***
Documentation    Tests for calculator
Library          CalculatorPy.py
Force Tags       Mathematical Action



*** Test Cases ***
Test addition 
    [Tags]   Addition
    add   5   5
    add   5   4
    add   6   17


Test subtraction 
    [Tags]   Subtraction
    sub    10     5
    sub    10     11


Test multiplication
    [Tags]   Multiplication
    mul    2     5
    mul    10    7


Test division 
    [Tags]   Division
    dive   5    5  
    dive   6    3

Test exponentiation
    [Tags]   Exponentiation
    power   6



