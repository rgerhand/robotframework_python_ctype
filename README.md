## Example of connection of RobotFramework together with Python and C code

The best experience of using the program will be on a Linux system.

If you want to run the .c file, you need the MinGW compiler.

##### Installation instructions:
* `sudo apt-get update -y `
* `sudo apt-get install -y mingw-w64`



##### To start the program in .c use the command:
*  `gcc calculator.c`

##### To call a .c program in Python it was necessary to create shared library file.
For this purpose, the command was used:
*  `cc -fPIC -shared -o [library.so] [file.c]`

It is also necessary to import the library:
*   `from ctypes import *`



##### You will need the Python compiler to compile
* sudo apt-get update
* sudo apt-get install python

##### The created library should then be used in a Python file 
* `so_file = "/home/user/library.so"`
*  `c_functions = CDLL(so_file)`

